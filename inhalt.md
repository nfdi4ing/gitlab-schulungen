# GitLab

## Gruppen

## Projekte
* leeres Projekt anlegen
* Projekt mit README anlegen
* Lizenz vergeben
* Projekt klonen -> weiter mit Git-Grundlagen

## Aufgaben
* Issues
* Milestones
* Releases

## Wiki
* Textformat [Markdown](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/) + [Gitlab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html#gitlab-flavored-markdown-gfm)

## Weitere Informationen
* [GitLab Docs](https://docs.gitlab.com/ee/README.html)

# Git
## Grundlagen
* set name and email
* clone 
* init
* status

## Versionieren I
* diff
* add
* commit
* log

## Versionieren II
* mv
* rm
* show
* reset
* tag
* checkout
* bisect

## Branchen
* branch
* merge
* rebase


## Verteilt arbeiten
* push
* pull
* fetch
* merge conflicts

## Weitere Informationen
* [Git Buch](https://git-scm.com/book/de/v2)


# Continuous Integration
