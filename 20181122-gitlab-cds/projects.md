# Projekte anlegen und konfigurieren

In GitLab können Sie Projekte für Ihre Daten und Quellcodes erstellen. Projekte bieten neben dem Git-Repository weitere Features, wie Aufgabenplanung, Wikis oder Continuous Integration. Ihre Projekte können öffentlich, intern oder privat verfügbar sein. Neben persönlichen Projekten haben Sie auch die Möglichkeit Gruppen anzulegen und Projekte im Rahmen dieser zu erstellen.

## Projekte Erstellen
1. Klicken Sie auf der Startseite auf die grüne Schaltfläche New Project oder verwenden Sie das Pluszeichen in der oberen rechten Ecke der Navigationsleiste.
2. Wählen Sie, ob Sie ein leeres Projekt oder mit einer der vordefinierten Projektvorlagen starten möchten. Durch Projektvorlagen können bestimmte Einstellungen automatisch vorgenommen werden. Sie können auch ein Projekt aus einem anderen Repository importieren, indem Sie auf die Registerkarte Import Project klicken.
3. Geben Sie die folgenden Informationen an: 
    * Geben Sie den Namen Ihres Projekts in das Feld Project Name ein. Sie können manche Sonderzeichen nicht als Namen verwenden, Leerzeichen, Bindestriche, Unterstriche oder sogar Emoji sind jedoch möglich.
    * Im Feld Project Description (optional) können Sie eine Beschreibung für das Dashboard Ihres Projekts eingeben, die anderen hilft zu verstehen, worum es bei Ihrem Projekt geht.
    * Das Visibility Level ändert die Anzeige- und Zugriffsrechte des Projekts für Benutzer.
    * Die Auswahl Initialize repository with a README erzeugt eine README-Datei und einen Entwicklungszweig in Ihrem Repository. Ansonsten ist das Repository leer.
4. Klicken Sie auf Create Project.

## Projekteinstellungen

Achtung: Nur Benutzer mit den Berechtigungen "Maintainer" oder "Owner" haben Zugriff auf Projekteinstellungen.

Sie ändern die Einstellungen, indem Sie auf der Projekthomepage auf Settings klicken.

### General

* General project settings: Passen Sie beschreibende Eigenschaften wie den Namen, die Beschreibung, den Projekt-Avatar an.
* Sharing and permissions: Setzen Sie Zugriffsberechtigungen für Ihr Projekt
* Issue settings: Bearbeiten Sie Vorlagen für das Issue Tracking
* Merge request settings: Definieren Sie, wie mit Merge Requests, dem Zusammenführen von Entwicklungszweigen, umgeganen wird.
* Export project: Exportieren Sie das gesamte Projekt von GitLab
* Advanced: Einstellungen zum Archivieren und Umbenennen von Projekten. Achtung: Diese Aktionen haben ggf. Seiteneffekte, da sich URLs für den Projektzugriff ändern.

### Projektmitglieder

Fügen Sie Projektmitglieder hinzu, um in einem Team zusammenzuarbeiten. Um Benutzer hinzuzufügen, müssen diese sich einmal in GitLab angemeldet haben.

Sie können Nutzer mit verschiedenen Berechtigungen hinzufügen:

* Guest: Lese- und Schreibzugirff auf den Issue Tracker, Lesezugriff auf das Wiki. Kein Zugriff auf Dateien im Repository!
* Reporter: Wie Gast aber mit Lesezugriff auf Dateien im Repository
* Developer: Wie Reporter aber mit Schreibzugriff auf das Repository
* Maintainer: Wie Developer aber mit Zugriff auf Projekteinstellungen

Eine ausführliche Beschreibung der Berechtigung finden Sie unter Permissions in der GitLab Hilfe.

### Repository

Bestimmen Sie, wie sich das Projektrepository bei Änderungen in verschiedenen Entwicklungszweigen verhält.

* Push to a remote repository: Replizieren Sie Änderungen an diesem Repository in ein anderes Repository
* Protected Branches und Protected Tags: Schützen Sie Entwicklungszweige vor versehentlichen Änderungen
* Deploy Keys und Deploy Token: Richten Sie Maschinenaccounts ein, die auf das Repository zugreifen können.

**Achtung**: Standardmäßig ist der Entwicklungszweig *master* als *Protected Branch* konfiguriert, sodass keine Änderungen gemacht werden können. Dies sollten Sie hier ändern.