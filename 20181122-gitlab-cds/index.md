# Management wissenschaftlicher Quellcodes und anderer Forschungsdaten - Grundlagen der Versionsverwaltung mit git und GitLab

Datum: 22.11.2018, 09:00 - 13:00

Format: Flipped Classroom in Moodle, ~1,5h Vorbereitungszeit

Links:
* http://www.rwth-aachen.de/go/id/ezno/file/5-15034/
* http://www.rwth-aachen.de/go/id/ohzl/file/5-15014/
* https://www1.elearning.rwth-aachen.de/course/view.php?id=78

## Inhalte

### Allgemeines

Herzlich willkommen zur online-Vorbereitung zum Gitlab-Workshop "Management wissenschaftlicher Quellcodes und anderer Forschungsdaten - Grundlagen der Versionsverwaltung mit git und GitLab"

#### Was ist Git/Gitlab?

Git ist eine etablierte Software zur Versionsverwaltung von Dateien. Wenn auch voranging zum Code-Management in der Softwareentwicklung genutzt, können in der Praxis nahezu jede Art von Datei per Versionsverwaltung nachverfolgt werden. Gut geeignet ist Git für flach strukturierte, textbasierte Datensätze wie csv, xml usw.

Die Weboberfläche GitLab ist eine Open-Source-Software, um eigene Git-Repositories selbst zu hosten und zu verwalten. Neben der Hauptaufgabe des Code-Managements werden noch andere Funktionalitäten wie ein einfaches Issue-Tracking-System, Wiki sowie Code-Review-Möglichkeiten abgedeckt. An der RWTH Aachen University steht Ihnen eine zentrale GitLab-Instanz zur Verfügung.

#### Online-Vorbereitung

In diesem Moodle-Raum stellen wir Ihnen Lernmaterialien zur Verfügung, die Sie dazu anleiten, Git auf Ihrem Rechner zu installieren, ein Projekt einzurichten und erste einfache Befehle auszuführen. Der online-Kurs gliedert sich in die Installation von Git, Git Basics zur Versionskontrolle und in eine Einführung in die Zusammenarbeit mit GitLab. Um alles auszuprobieren sollten Sie bis zu einen Vormittag Zeit einplanen. Sollten Sie während Ihrer online-Vorbereitung Fragen haben, können Sie diese im Forum stellen. Wir werden versuchen, diese dann zeitnah zu beantworten.

Am Ende der online-Vorbereitung sollten Sie Git auf Ihrem Rechner installiert, ein erstes Projekt angelegt und erste einfache Befehle mit Git ausgeführt haben. Sie haben sich auf der GitLab Oberfläche der RWTH eingeloggt und sollten in der Lage sein, mit GitLab loslegen zu können.

Im Workshop werden Sie weitere Git/GitLab-Funktionalitäten kennenlernen. Gemeinsam werden wir Anwendungsbeispiele diskutieren und Übungen zu individuellen Arbeitsabläufe und Best Practices durchführen. Bitte bringen Sie dafür Ihren Laptop mit der installierten git-Software und eigene Daten mit.


### Installation von Git

* [Installation, Mac OSX (2:49)](https://www.youtube.com/watch?v=TvrZw47e7tI)
* [Installation, Windows (1:53)](https://www.youtube.com/watch?v=5KFn0r2XrtA)
* [Installationsanleitung, Windows](install_windows.md)
* [Installation, Linux (2:13)](https://www.youtube.com/watch?v=HSOuBmiCdrM)
* [Konfigurieren (2:46)](https://www.youtube.com/watch?v=PegV5zz5iFU)
* [Repository Anlegen, Mac OSX (2:51)](https://www.youtube.com/watch?v=Bo-pKqHO2go)
* [Repository Anlegen, Windows (6:34)](https://www.youtube.com/watch?v=8Qau5_NmF9s)

### Git Basics Versionskontrolle

* [Änderungen Machen (3:13)](https://www.youtube.com/watch?v=0ya5jueUlqs)
* [Änderungen Überprüfen (1:15)](https://www.youtube.com/watch?v=XyrnJNrI3cQ)
* [Änderungen Committen (3:02)](https://www.youtube.com/watch?v=mg82bvto4Ug)
* [Versionen Vergleichen (2:50)](https://www.youtube.com/watch?v=-9hyURYmvsY)
* [Änderungen Verwerfen (2:18)](https://www.youtube.com/watch?v=ch-VjQW6tsg)
* [Entwicklungszweige anlegen (6:00)](https://www.youtube.com/watch?v=tHtiehTr59I)
* [Dateien löschen/entfernen (2:17)](https://www.youtube.com/watch?v=QHBTc92WT1E)
* [Entwicklungszweige zusammenführen (6:28)](https://www.youtube.com/watch?v=xyTS2yyOWnA)

### Zusammenarbeit mit GitLab

* [Login in GItLab und Profileinstellungen](login-and-profile.md)
* [Projekte anlegen und konfigurieren](projects.md)
* [Dokumentation, Markdown und Wikis](documentation.md)
* [Projekte und Arbeitspakete planen und verfolgen](issues.md)
* [Codeschnipsel](snippets.md)

#### Aufgaben

##### Profileinstellungen

- [ ] Loggen Sie sich bei GitLab ein Mit diesem Element verbundender Link
- [ ] Öffnen Sie die Profileinstellungen
- [ ] Überprüfen Sie die Angaben unter den Menüpunkten Account und Profile
- [ ] Setzen Sie im Menupunkt Password ein Passwort um mit HTTPS arbeiten zu können

##### Peojekt Erstellen

- [ ] Loggen Sie sich bei GitLab ein Mit diesem Element verbundender Link
    - [ ] Erstellen Sie ein neues Projekt mit dem Namen "workshop"
    - [ ] Initialisieren Sie das Projekt mit einer README Datei
    - [ ] Klicken Sie auf die README Datei um den Quellcode anzusehen
    - [ ] Klicken Sie auf den Button "Edit" und machen Sie eine Änderung an der README Datei im Browser
    - [ ] Gehen Sie zurück auf die Projektseite und kopieren sie die URL zum Herunterladen des Projekts. Achten Sie darauf, die HTTPS URL zu kopieren
- [ ] Erstellen Sie eine lokale Kopie um mit dem Repository zu arbeiten
    - [ ] Öffnen Sie die Kommandozeile um mit Git auf ihrem Computer zu arbeiten.
    - [ ] Erstellen Sie einen neuen, leeren Ordner. Zum Beispiel mit dem Kommando "mkdir workshop"
    - [ ] Wechseln Sie in den Ordner mit dem Kommando "cd workshop"
    - [ ] Laden Sie das Projekt mit dem Kommando "git clone [URL]" herunter
- [ ] Machen Sie eine Änderung in der README Datei, diesmal in der lokalen Kopie des Repositories
    - [ ] Committen Sie die Änderung in ihrer lokalen Kopie.
    - [ ] Übertragen sie die Änderung mit dem Kommando "git push origin master" an GitLab
    - [ ] Überprüfen Sie im Browser ob die Änderung in GitLab angekommen ist.

### Weiterführende Links

* [Linux Terminal Server Emulator](https://cs-education.github.io/sys/#VM)
* [GitLab der RWTH Aachen](https://git.rwth-aachen.de/)
* [Version Control with Git](http://swcarpentry.github.io/git-novice/01-basics/index.html)

## Kursbeschreibung

### Erläuterung

Versionskontrolle ist wie ein Laborbuch für Textdaten: Programmiererinnen und Programmierer nutzen seit langem Techniken, um den Überblick zu behalten, den Arbeitsprozess zu dokumentieren und mit Kolleginnen und Kollegen zusammenzuarbeiten. Alles, was sich im Laufe der Zeit in den Dateien ändert oder ausgetauscht wird, lässt sich mit einem Versionskontrollsystem verwalten. Das gilt nicht nur für Programmcodes, sondern auch für Skripte, Konfigurationsdateien, LaTeX Dokumente, CSV Datensätze oder andere Datenbestände in Textform, die vielfältig Grundlage für Forschungserkenntnisse sind. Versionskontrolle bringt viele Vorteile in der Arbeit mit Textdateien:

Was einmal an das Versionskontrollsystem übergeben wurde, geht nicht verloren. Alte Versionen können jederzeit wiederhergestellt werden. Versionskommentare helfen den Überblick zu behalten.

Das Versionskontrollsystem zeigt, wer welche Änderungen gemacht hat, sodass Ansprechpartner gefunden und Rückfragen direkt adressiert werden können.

In der Zusammenarbeit verhindert das Versionskontrollsystem, dass versehentlich Änderungen von anderen überschrieben werden.

### Ziel

* Sie verstehen, wie Versionskontrollsysteme Ihren Arbeitsablauf unterstützen
* Sie kennen git als Versionskontrollsystem und können es einrichten
* Sie können git alleine und kollaborativ zur Versionsverwaltung von Textdaten nutzen
* Sie kennen Projektmanagement- und Kollaborationsfeatures von GitLab, wie z.B. Wikis, Aufgaben und Meilensteine

### Methoden

Flipped Classroom: Etwa 4 Wochen vor dem Workshop werden Ihnen in einem Moodle-Lernraum online-Lehrmaterialien zur Einführung in die Grundlagen bereitgestellt. Während der Präsenzzeit werden individuelle Arbeitsabläufe und Best Practices diskutiert und vermittelt.

### Inhalt

In diesem Seminar vermitteln wir Ihnen die Grundlagen in der Arbeit mit dem Versionskontrollsystem git und der Weboberfläche GitLab. Gemeinsam mit Ihnen erarbeiten wir Antworten auf folgende Leitfragen:

* Was ist Versionskontrolle?
* Wie wird git eingerichtet?
* Wie werden git-Repositorien erstellt?
* Wie arbeitet man mit der git Versionshistorie?
* Wie können mit Hilfe von GitLab Daten geteilt werden?
* Wie werden Versionskonflikte aufgelöst?
* Wie kann Versionskontrolle über GitLab das Projektmanagement unterstützen?

In dem Seminar möchten wir praxisnah mit Ihnen arbeiten und auf Ihre individuellen Problemstellungen eingehen. Bitte bringen Sie dafür Ihren Laptop mit der installierten git-Software und eigene Daten mit. Mit den online-Lehrmaterialien erhalten Sie einen Link mit den weiteren Informationen zur Installation und der benötigten Software. Optional können Sie auch einen unserer Schulungslaptops nutzen.

### Zielgruppen
* Doktorandinnen und Doktoranden, die im Center for Doctoral Studies registriert sind.
* Postdocs, der akademische Mittelbau, Juniorprofessorinnen und Juniorprofessoren.