# Login in GitLab und Profileinstellungen

## Erste Anmeldung

Rufen Sie die Web-Seite https://git.rwth-aachen.de/ auf und wählen Sie zum Einloggen (Sign In) Shibboleth aus:

![](./img/gitlab_signin.png)

Sie kommen zur DFN-AAI-Anmeldeseite. Wählen Sie aus der Dropdown Liste RWTH Aachen University aus:

![](./img/gitlab_dfn_wayf.png)

Melden Sie sich mit Ihrer User-ID und dem dazugehörigen Passwort an.

Sie befinden sich jetzt auf der GitLab Startseite

## Profileinstellungen

Öffnen Sie die Profileinstellungen über den Eintrag Settings im Menu, das erscheint, wenn Sie auf das Personen-Symbol in der rechten oberen Ecke klicken:

![](./img/gitlab_profile_settings.png)

Im Menü auf der Linken Seite können Sie verschiedene Bereiche Ihres Profils auswählen. Sie sollten die unter Profile und Account angezeigten Informationen prüfen und ggf. korrigieren.

Bevor Sie mit einem Repository auf dem GitLab-Server arbeiten können, müssen Sie ein Passwort oder einen SSH Schlüssel in Ihrem Profil hinterlegen. Ansonsten zeigt Ihnen GitLab in Ihrem Profil eine Warnung an. Gehen Sie dazu in Ihren Profileinstellungen auf den Menüpunkt Password. Das dort gesetzte Passwort müssen Sie verwenden, wenn Sie mit der Git Kommandozeile oder anderen Clients auf ein Repository zugreifen.

![](./img/gitlab_profile_password.png)

Alternativ können Sie einen SSH Schlüssel in Ihrem Profil hinterlegen. Der genaue Prozess zum erzeugen eines SSH Schlüssels wird in der GitLab Hilfe erklärt. Im Rahmen des Workshops sowie für die meisten späteren Anwendungszenarien ist die Verwendung eines Passworts aber völlig ausreichend.