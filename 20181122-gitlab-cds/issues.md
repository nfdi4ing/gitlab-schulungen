# Projekte und Arbeitspakete planen und verfolgen

Der GitLab Issue Tracker ist ein fortschrittliches und vollständiges Werkzeug zur Verfolgung der Entwicklung einer neuen Idee oder des Prozesses der Problemlösung.

Es ermöglicht Ihnen, Ihrem Team und Ihren Mitarbeitern, Vorschläge vor und während der Implementierung auszutauschen und zu diskutieren. EIn Problem oder eine Idee wird als Issue in GitLab angelegt.

## Anwendungsfälle

Issues können viele Anwendungen haben. Dies sind einige Fälle, in denen das Erstellen von Issues am häufigsten verwendet wird:

* Besprechung der Umsetzung einer neuen Idee
* Einreichen von Feature-Vorschlägen
* Fragen stellen
* Meldung von Fehlern und Fehlfunktionen
* Unterstützung einholen
* Ausarbeitung neuer Code-Implementierungen
* Siehe auch den Blogbeitrag "Always start a discussion with an issue".

# Issue Tracker

Der Issue Tracker ist die Sammlung von offenen (open) und geschlossenen (closed) Issues, die in einem Projekt erstellt wurden. Es ist für alle Projekte verfügbar.

Sie finden den Issue Tracker, indem Sie auf der Startseite Ihres Projekts > Issues navigieren.

## Issue Board

Das GitLab Issue Board ist eine Möglichkeit, um Ihren Workflow zu verbessern, indem Sie Issues in GitLab organisieren und priorisieren.

Sie finden GitLab Issue Boards, indem Sie auf der Startseite Ihres Projekts > Issues > Board navigieren.

## Issues verwalten
Ein Issue beginnt mit seinem Status (offen oder geschlossen), gefolgt von seinem Autor, und bietet die Möglichkeit viele andere Informationsfelder zu pflegen. Viele der Felder, wie Titel und Beschreibung, werden automatisch aktualisiert, wenn sie von einem anderen Benutzer geändert werden.

Im folgenden werden einige Funktionen anhand der Nummerierung im folgenden Bild erklärt.

![](./img/issues_main_view_numbered.jpg)

### 1. Neuen Issue anlegen, schließen, bearbeiten

New Issue: Erstellen Sie eine neue Issue im selben Projekt.

Close Issue: Diesen Issue schließen

Edit: Bearbeiten Sie die Felder, die Ihnen zur Verfügung stehen, wenn Sie ein Issue erstellen.

### 2. Todos

Add Todo: Füge dieses Problem zu deiner GitLab Todo-Liste hinzu.

Mark done: Markieren Sie das Problem in der Todo-Liste als erledigt.

### 3. Assignee

Wenn jemand anfängt, an einem Problem zu arbeiten, kann es der Person zugewiesen werden. Die Idee ist, dass der Assignee für den Issue verantwortlich ist. Benutzer müssen Mitglied des Projektes sein oder den Issue angelegt haben, um als verantwortlich eingetragen werden zu können.

### 4. Meilensteine

Zusammengehörige Issues können in Meilensteinen zusammengefasst werden. Wählen Sie einen Meilenstein aus, dem Sie diesen Issue zuordnen möchten.

### 5. Zeiterfassung

Estimate: Füge eine Schätzzeit hinzu, die benötigt wird, um den Issue zu schließen.

Spend: Füge die Zeit hinzu, die für die Untersuchung des Issues aufgewendet wurde.

### 6. Fälligkeitsdatum

Wenn Sie nach einem engen Zeitplan arbeiten, ist es wichtig, dass Sie eine Möglichkeit haben, eine Frist für die Umsetzung und Lösung von Issues festzulegen. Dies kann durch das Fälligkeitsdatum erleichtert werden. Fälligkeitsdaten können beliebig oft geändert werden.

### 7. Labels

Kategorisieren Sie Probleme, indem Sie ihnen Labels geben. Labels haben eine Beschriftung und eine Farbe und sorgen so für eine schnelle Übersicht in Issues. Sie helfen beim Organisieren der Arbeitsabläufe des Teams. 

Wenn das gewünschte Label noch nicht existiert, öffnet es beim Klicken auf Bearbeiten ein Dropdown-Menü, in dem Sie Neue Label erstellen auswählen können.

### 9. Teilnehmer

Alle Personen, die an diesem Thema beteiligt sind (in der Beschreibung oder in der Diskussion erwähnt).

### 10. Benachrichtigungen

Subscribe: Wenn Sie kein Teilnehmer der Diskussion zu diesem Thema sind, aber Benachrichtigungen über jede Änderung am Issue erhalten möchten, abonnieren Sie dieses Issue. Sie werden dann per E-Mail über Änderungen benachrichtigt.

Abmelden: wenn Sie nicht länger Benachrichtigungen zu diesem Thema erhalten möchten, können sie diese abbestellen.

### 11. Referenz

Eine schnelle "Kopieren in die Zwischenablage"-Schaltfläche zur Referenz dieses Issues, foo/bar#xxx, wobei foo der Benutzername oder Gruppenname, bar der Projektname und xxx die Issue-Nummer ist.

### 12. Titel und Beschreibung

Der Titel ist ein einfacher Texttitel, der das Thema des Issues beschreibt. Das Beschreibungsfeld enthält eine ausführlichere Beschreibung und kann mit Markdown formatiert werden.

### 13. @mentions

Sie können mit @username einen Benutzer oder eine Gruppe erwähnen. Der Nutzer wird dann in GitLab in der ToDo Liste und per E-Mail benachrichtigt, es sei denn, diese Person hat alle Benachrichtigungen in ihren Profileinstellungen deaktiviert.

Um Ihre Benachrichtigungseinstellungen zu ändern, navigieren Sie zu Profileinstellungen > Benachrichtigungen > Globale Benachrichtigungsebene und wählen Sie Ihre Einstellungen aus dem Dropdown-Menü.

Der Sonderfall @all ist für alle Mitgleider eines Projekts. Um keine unnötigen (spam) E-Mails zu versenden, sollte dies aber möglicht vermieden werden.

### 14. Zugehörige Merge Requests

Änderungen auf einem Entwicklungszweig können an ein Issue angehängt werden, indem diese in der Beschreibung erwähnt werden.

### 15. Emojis Vergeben

Vergeben Sie ein Emoji für dieses Thema.  Ein Emoji zu verleihen ist eine Möglichkeit, kurzes Feedback über Ihre Meinung zu einem Issue zu geben.

### 16. Kommentare

Arbeiten Sie an diesem Issue mit, indem Sie Kommentare schreiben. Diese Textfelder können ebenfalls Markdown-formatierten Text anzeigen.

### 17. Kommentieren, eine Diskussion starten oder kommentieren und abschließen

Sobald Sie einen Kommentar geschrieben haben, können Sie:

Klicken Sie auf Comment und Ihr Kommentar wird veröffentlicht.

Klicken Sie auf "Start Discussion": Starten Sie einen neuen Thread innerhalb des Issues, um bestimmte Punkte zu diskutieren.

Klicken Sie auf "Comment and close issue": Geben Sie Ihren Kommentar ab und schließen Sie das Problem mit einem Klick.

### 18. Neuer Merge Request

Erstellen Sie einen neuen Merge Request (mit einem neuen Entwicklungszweig, der nach dem Issue benannt ist) in einer Aktion. Wird die Merge Request angenommen, wird automatisch auch der Issue geschlossen. 